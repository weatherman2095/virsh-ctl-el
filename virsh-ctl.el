;;; virsh-ctl.el --- Simple virsh-based VM power control for Emacs  -*- lexical-binding: t; -*-

;; This file is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file. If not, see <https://www.gnu.org/licenses/>.

(require 'cl-lib)
(require 'tabulated-list)

(defvar virsh-ctl-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map tabulated-list-mode-map)
    (define-key map "s" 'virsh-ctl--toggle-node-status)
    (define-key map "Q" 'kill-this-buffer)
    map)
  "Local keymap for `virsh-ctl-mode' buffers.")

(defvar virsh-ctl--uri-format "*virsh-ctl %s*"
  "Format string used to create buffer name from a uri")

(defvar-local virsh-ctl--uri nil
  "user@host uri for the current buffer's SSH interactions")

(define-derived-mode virsh-ctl-mode tabulated-list-mode "Virsh Control"
  "Major mode for browsing Libvirt node listings on remote hosts")

(defun virsh-ctl--check-conditions (user host)
  "Check preconditions for `virsh-ctl' to work with provided
environment."
  (let ((uri (format "%s@%s" (string-trim user) (string-trim host))))
    (cl-flet ((check-ssh ()
                         (= (call-process "ssh" nil nil nil
                                          uri "exit")
                            0))
              (check-sudo ()
                          (= (call-process "ssh" nil nil nil
                                           uri "sudo" "who")
                             0))
              (check-virsh ()
                           (= (call-process "ssh" nil nil nil
                                            uri "sudo" "virsh" "--version")
                              0)))
      (let ((ssh-error (format "virsh-ctl: ssh connection as %s failed" uri))
            (sudo-error (format "virsh-ctl: sudo over ssh connection %s failed" uri))
            (virsh-error (format "virsh-ctl: sudoed virsh as %s failed" uri)))
        (cl-assert (check-ssh) nil ssh-error)
        (cl-assert (check-sudo) nil sudo-error)
        (cl-assert (check-virsh) nil virsh-error)))))

(defun virsh-ctl--get-list-all-sync (uri &optional inactive)
  "Get a list of VM names from `uri', active by default and
inactive if `inactive' is `t'"
  (cl-check-type inactive boolean)
  (let ((list-str
         (with-temp-buffer
           (apply #'call-process
                  "ssh" nil t nil
                  (append (list uri "--"
                                "sudo" "virsh" "list" "--name")
                          (when inactive
                            '("--inactive"))))
           (buffer-string))))
    (split-string list-str "\n" t)))

(defun virsh-ctl--vms-from-remote ()
  "Get virsh VMs' state from remote for inactive & running VMs
formatted for use in `tabulated-list-entries'"
  (let (hosts)
    (dolist (host (virsh-ctl--get-list-all-sync virsh-ctl--uri))
      (push (cons host "running") hosts))
    (dolist (host (virsh-ctl--get-list-all-sync virsh-ctl--uri t))
      (push (cons host "inactive") hosts))
    (cl-loop for host in hosts
             for name = (car host)
             for status = (cdr host)
             collect (list name (vector name status)))))

(defun virsh-ctl--update-data ()
  "Update virsh data from buffer's corresponding remote host"
  (setf tabulated-list-entries (virsh-ctl--vms-from-remote)))

(defun virsh-ctl--initialize-host-virsh (buf-name)
  "Initialize virsh state data & display in `buf-name'"
  (with-current-buffer (get-buffer-create buf-name)
    (setf tabulated-list-format [("name" 20 t)
                                 ("status" 20 t)]
          tabulated-list-sort-key '("name" . nil))
    (add-hook 'tabulated-list-revert-hook #'virsh-ctl--update-data nil t)
    (tabulated-list-init-header)
    (revert-buffer)
    (setf (point) (point-min))))

(defun virsh-ctl (user host)
  "Provide a control interface to `virsh' on a remote `host'
managed sudo-enabled `user'"
  (interactive "sSudoer User: \nsHost: ")
  (virsh-ctl--check-conditions user host)
  (let* ((uri (format "%s@%s" (string-trim user) (string-trim host)))
         (buf-name (format virsh-ctl--uri-format uri)))
    (with-current-buffer (get-buffer-create buf-name)
      (virsh-ctl-mode)
      (setf virsh-ctl--uri uri)
      (virsh-ctl--initialize-host-virsh buf-name))
    (pop-to-buffer buf-name)))

(defun virsh-ctl--toggle-node-status ()
  "Toggle a virsh VM between running and inactive states"
  (interactive)
  (let* ((name (tabulated-list-get-id))
         (state (aref (tabulated-list-get-entry) 1))
         (state-operand (pcase state
                          ("running" "shutdown")
                          ("inactive" "start"))))
    (call-process "ssh" nil nil nil
                  virsh-ctl--uri "--"
                  "sudo" "virsh" state-operand name)
    (tabulated-list-set-col 1 "pending" t)))

(provide 'virsh-ctl)
